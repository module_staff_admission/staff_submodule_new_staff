const request = require('request');
const async = require('async');
const apiOptions = require('../../api_options');
const details_list = require('./details_list');
const userDetailsRequest = details_list.userDetailsRequest;
const selectedStaffDetailsRequest = details_list.selectedStaffDetailsRequest;

/* GET home page. */
const viewStaffPage = (req, res) => {
    const path = '/staff';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'GET',
        json: {},
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 404) {
                body = null;
            }
            res.render('new_staff/view_staff', 
            { 
                title: 'View Staff',
                staffs: body
            });
        }
    );
};

const doAddNewStaff = (req, res) => {
    const postdata = {
        position_id: req.body.position_id,
        dept_id: req.body.dept_id,
        name: req.body.name,
        contact_no: req.body.contact_no,
        secondary_email: req.body.secondary_email,
        home_address: req.body.home_address,
        postcode: req.body.postcode,
        IC: req.body.IC,
        nation_id: req.body.nation_id,
        gender_id: req.body.gender_id,
        religion_id: req.body.religion_id,
        marriage_status_id: req.body.marriage_status_id,
        DOB: req.body.DOB,
        StateOB_id: req.body.StateOB_id,
        race_id: req.body.race_id,
        date_enter: req.body.date_enter,
        employee_status_id: req.body.employee_status_id
    };
    const path = '/staff';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'POST',
        json: postdata
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 201) {
                res.redirect('/view_staff');
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
};


const doUpdateStaff = (req, res) => {
    const postdata = {
        position_id: req.body.position_id,
        dept_id: req.body.dept_id,
        name: req.body.name,
        contact_no: req.body.contact_no,
        secondary_email: req.body.secondary_email,
        home_address: req.body.home_address,
        postcode: req.body.postcode,
        IC: req.body.IC,
        nation_id: req.body.nation_id,
        gender_id: req.body.gender_id,
        religion_id: req.body.religion_id,
        marriage_status_id: req.body.marriage_status_id,
        DOB: req.body.DOB,
        StateOB_id: req.body.StateOB_id,
        race_id: req.body.race_id,
        date_enter: req.body.date_enter,
        employee_status_id: req.body.employee_status_id
    };
    const path = `/staff/${req.body.staff_id}`;
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'PUT',
        json: postdata
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 200 || response.statusCode === 204) {
                res.redirect('/view_staff');
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
};

const doDeleteStaff = (req, res) => {
    const path = `/staff/${req.body.staff_id}`;
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'DELETE'
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 200 || response.statusCode === 204) {
                res.redirect('/view_staff');
            }
            else {
                res.redirect('/view_staff');
            }
        }
    );
}

const addNewStaffPage = (req, res) => {
    async.parallel(userDetailsRequest, (err, results) => {
        res.render('new_staff/add_staff', { 
            title: 'Add New Staff',
            nation_list: results.nation_list,
            local_state_list: results.local_state_list,
            gender_list: results.gender_list,
            religion_list: results.religion_list,
            race_list: results.race_list,
            marriage_status_list: results.marriage_status_list,
            employee_status_list: results.employee_status_list,
            staff_position_list: results.staff_position_list,
            office_dept_list: results.office_dept_list
        });
    }
    );
}

const updateStaffPage = (req, res) => {
    staff_id = req.params.staff_id;
    userDetailsRequest['staff'] = (next) => {
        const path = `/staff/${staff_id}`;
        const requestOptions = {
            url: `${apiOptions.server}${path}`,
            method: 'GET',
            json: {},
        };
        request(
            requestOptions,
            (err, response, body) => {
                next(null, body);
            }
        );
    }
    async.parallel(userDetailsRequest, (err, results) => {
        res.render('new_staff/update_staff', { 
            title: 'Update Staff',
            staff: results.staff,
            nation_list: results.nation_list,
            local_state_list: results.local_state_list,
            gender_list: results.gender_list,
            religion_list: results.religion_list,
            race_list: results.race_list,
            marriage_status_list: results.marriage_status_list,
            employee_status_list: results.employee_status_list,
            staff_position_list: results.staff_position_list,
            office_dept_list: results.office_dept_list
        });
    }
    );
}


module.exports = {
    viewStaffPage,
    addNewStaffPage,
    updateStaffPage,
    doAddNewStaff,
    doUpdateStaff,
    doDeleteStaff
};