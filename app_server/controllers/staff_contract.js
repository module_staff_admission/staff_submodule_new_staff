const request = require('request');
const async = require('async');
const apiOptions = require('../../api_options')
const details_list = require('./details_list');
const userDetailsRequest = details_list.userDetailsRequest;
const selectedStaffDetailsRequest = details_list.selectedStaffDetailsRequest;

/* GET home page. */
const viewStaffPage = (req, res) => {
    const path = '/staff';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'GET',
        json: {},
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 404) {
                body = null;
            }
            res.render('staff_contract/view_staff', 
            { 
                title: 'View Staff',
                staffs: body
            });
        }
    );
};

const addContractPage = (req, res) => {
    staff_id = req.params.staff_id;

    const requests = {
        staff: (next) => {
            const path = `/staff/${staff_id}`;
            const requestOptions = {
                url: `${apiOptions.server}${path}`,
                method: 'GET',
                json: {},
            };
            request(
                requestOptions,
                (err, response, body) => {
                    if (response.statusCode === 404) {
                        body = null;
                    }
                    next(null, body);
                }
            );
        },
        staff_contract: (next) => {
            const path = `/staff_contract/${staff_id}`;
            const requestOptions = {
                url: `${apiOptions.server}${path}`,
                method: 'GET',
                json: {},
            };
            request(
                requestOptions,
                (err, response, body) => {
                    if (response.statusCode === 404) {
                        body = null;
                    }
                    next(null, body);
                }
            );
        }
    };

    async.parallel(requests, (err, results) => {
        res.render('staff_contract/add_contract', { 
            title: 'View or Add Contract',
            staff: results.staff,
            staff_contract: results.staff_contract
        });
    }
    );
}


const updateContractPage = (req, res) => {
    _id = req.params._id;
    let staff_id;

    const requests = {
        staff_contract: (next) => {
            const path = `/staff_contract_single/${_id}`;
            const requestOptions = {
                url: `${apiOptions.server}${path}`,
                method: 'GET',
                json: {},
            };
            request(
                requestOptions,
                (err, response, body) => {
                    if (response.statusCode === 404) {
                        body = null;
                    }
                    staff_id = response.body[0].staff_id;
                    next(null, body);
                }
            );
        },
        staff: (next) => {
            const path = `/staff/${staff_id}`;
            const requestOptions = {
                url: `${apiOptions.server}${path}`,
                method: 'GET',
                json: {},
            };
            request(
                requestOptions,
                (err, response, body) => {
                    if (response.statusCode === 404) {
                        body = null;
                    }
                    next(null, body);
                }
            );
        }
    };

    // limit to 1 to fake sequential, lazy to search for more docs, sorry
    async.parallelLimit(requests, 1, (err, results) => {
        res.render('staff_contract/update_contract', { 
            title: 'Update Contract',
            staff: results.staff,
            staff_contract: results.staff_contract[0]
        });
    }
    );
}

const doAddContract = (req, res) => {
    const postdata = {
        staff_id: req.params.staff_id,
        date_start: req.body.date_start,
        date_end: req.body.date_end,
        status: req.body.status,
        salary: req.body.salary
    };
    const path = '/staff_contract';
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'POST',
        json: postdata
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 201) {
                res.redirect('/staff_contract');
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
}

const doUpdateContract = (req, res) => {
    const postdata = {
        status: req.body.status,
        salary: req.body.salary
    };
    const path = `/staff_contract_single/${req.params._id}`;
    const requestOptions = {
        url: `${apiOptions.server}${path}`,
        method: 'PUT',
        json: postdata
    };
    request(
        requestOptions,
        (err, response, body) => {
            if (response.statusCode === 200 || response.statusCode === 204) {
                res.redirect(`/staff_contract/add_contract/${req.body.staff_id}`);
            }
            else {
                showError(req, res, response.statusCode);
            }
        }
    );
}

module.exports = {
    viewStaffPage,
    addContractPage,
    updateContractPage,
    doAddContract,
    doUpdateContract
};