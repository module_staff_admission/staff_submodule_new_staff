var express = require('express');
var router = express.Router();
const ctrlMain = require('../controllers/main');

/* GET home page. */
router.get('/', ctrlMain.viewStaffPage);
router.get('/view_staff', ctrlMain.viewStaffPage);
router.route('/add_staff')
    .get(ctrlMain.addNewStaffPage)
    .post(ctrlMain.doAddNewStaff);
router.route('/update_staff/:staff_id')
    .get(ctrlMain.updateStaffPage)
    .post(ctrlMain.doUpdateStaff);

router.route('/delete_staff')
    .post(ctrlMain.doDeleteStaff);

module.exports = router;
