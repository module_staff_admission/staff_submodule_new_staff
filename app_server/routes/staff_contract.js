var express = require('express');
var router = express.Router();
const ctrlMain = require('../controllers/main');
const ctrlStaffContract = require('../controllers/staff_contract')

router.get('/', ctrlStaffContract.viewStaffPage);
router.route('/add_contract/:staff_id')
    .get(ctrlStaffContract.addContractPage)
    .post(ctrlStaffContract.doAddContract);

router.route('/update_contract/:_id')
    .get(ctrlStaffContract.updateContractPage)
    .post(ctrlStaffContract.doUpdateContract);

module.exports = router;